@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Producto</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    

                    <div class="row">
                        
                        <div class="card col-lg-4">
                            <img class="card-img-top p-2" src="{{ $product->photo }}" alt="Card image cap" width="300" height="200">
                            <div class="card-body">
                                <h5 class="card-title">{{ $product->name }}</h5>
                                <p class="card-text">{{ $product->description }}</p>
                                <a href="#" class="btn btn-primary">$ {{ $product->price }}</a>
                            </div>

                            <a href="{{ url('add-to-cart/' . $product->id) }}" class="btn btn-primary btn-lg btn-block">Agregar al carrito</a>
                            
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
