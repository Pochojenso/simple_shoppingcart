@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Carrito</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    

                    <?php $valor = 0; ?>

                    @if( session('cart') )
                    <table class="table">
                        <thead class="thead-info">
                            <tr>
                                <th scope="col">Producto</th>
                                <th scope="col">Precio Unitario</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Precio Total</th>
                                <th scope="col">Foto</th>
                            </tr>
                        </thead>
                      <tbody>
                        @foreach ( session('cart') as $id  => $details)
                            <?php $valor += $details['price'] * $details['quantity'] ?>

                            <tr>

                              <td>
                                  {{ $details['name'] }}
                              </td>
                              <td>
                                  {{ $details['price'] }}
                              </td>
                              <td>
                                  {{ $details['quantity'] }}
                              </td>
                              <td>
                                  {{ $details['price'] * $details['quantity'] }}
                              </td>
                              <td>
                                  <img src="{{ $details['photo'] }}" width="50" height="50">
                              </td>

                            </tr>
                        @endforeach

                      </tbody>

                    </table>
                    @else
                    
                        <h1 class="text-center">{{ __('No hay productos en el carrito') }}</h1>
                            
                    @endif

                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <h6>Subtotal: <b>$ {{ $valor }}</b></h6>
                        <h6>IVA: <b>$ {{ $valor*0.21 }}</b></h6>
                        <h6 class="text-info">Total: <b>$ {{ $valor+$valor*0.21 }}</b></h6>
                    </div>
                <div>
            </div>
        </div>
    </div>
</div>
@endsection
