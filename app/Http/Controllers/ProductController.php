<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
    	$products = Product::all();

    	return view('products.products', compact('products'));
    }

    public function detail($id)
    {
    	$product = Product::find($id);

    	return view('products.detail')->with('product', $product);
    }

    public function cart()
    {
    	return view('products.cart');
    }

    public function addToCart($id)
    {
    	// Logica para agregar producto al carrito
    	$product = Product::find($id);
    	$cart = session()->get('cart');

    	// si no hay producto en el carrito
    	if (!$cart) {

    		$cart = [
    			$id = [
    				"name" => $product->name,
    				"quantity" => 2,
    				"price" => $product->price,
    				"photo" => $product->photo
    			]
    		];

    		session()->put('cart', $cart);

    		return redirect()->back()->with('success', 'Product added to cart successfully!');
    	}

    	if (isset($cart[$id])) {
    		
    		$cart[$id]['quantity']++;

    		session()->put('cart', $cart);

    		return redirect()->back()->with('success', 'Product added to cart successfully!');
    	}

    	$cart[$id] = [
			"name" => $product->name,
			"quantity" => 2,
			"price" => $product->price,
			"photo" => $product->photo
		];

		session()->put('cart', $cart);

		return redirect()->back()->with('success', 'Product added to cart successfully!');
    }
}
